<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
})->name('home');

Route::group(['prefix' => 'dashboard','middleware' => ['auth']],function (){
    Route::get('/', 'DashboardController@index')->name('dashboard');
    Route::get('/university', 'DashboardController@universitySetup')->name('university-setup');
    Route::get('/staff-registration', 'DashboardController@staffRegistration')->name('staff-registration');
    Route::get('/student-registration', 'DashboardController@studentRegistration')->name('student-registration');
    Route::get('/staffboard', 'DashboardController@staffboard')->name('staffboard');
    Route::get('/staffboard/{staffboard}', 'DashboardController@staffboardMessage')->name('per-staffboard');
    Route::get('/messages', 'DashboardController@messages')->name('messages');
    Route::get('/messages/{message}', 'DashboardController@message')->name('per-messages');
    Route::get('/announcements', 'DashboardController@announcements')->name('announcements');
    Route::get('/notes', 'DashboardController@notes')->name('notes');
    Route::get('/assignments', 'DashboardController@assignments')->name('assignments');
    Route::get('/course-work', 'DashboardController@courseWork')->name('course-work');
    Route::get('/auto-save', 'DashboardController@autoSave')->name('auto-save');

//    DM
    Route::post('/sendmessage', 'DirectMessageController@sendMessage');
    Route::get('/conversation/{profile_id}', 'DirectMessageController@getConversation');
});

//Resource routes
Route::resource('university-type', 'UniversityTypeController');
Route::resource('university', 'UniversityController');
Route::resource('faculty', 'FacultyController');
Route::resource('course', 'CourseController');
Route::resource('subject', 'SubjectController');
Route::resource('subject-post', 'SubjectPostController');
Route::resource('subject-category', 'SubjectCategoryController');
Route::resource('semester', 'SemesterController');
Route::resource('role', 'RoleController');
Route::resource('user', 'UserController');
Route::resource('role_user', 'RoleUserController');
Route::resource('profile', 'ProfileController');
Route::resource('profile_subject', 'ProfileSubjectController');
Route::resource('course_profile', 'CourseProfileController');
Route::resource('category', 'CategoryController');
Route::resource('media_type', 'MediaTypeController');
Route::resource('forum_type', 'ForumTypeController');
Route::resource('resource', 'ResourceController');
Route::resource('news', 'NewController');
Route::resource('forum', 'ForumController');
Route::resource('association', 'AssociationController');

//api
Route::prefix('api')->group(function () {
   Route::get('profiles', 'ApiController@profiles')->name('api_profiles');
   Route::get('profile/{profile}', 'ApiController@profile')->name('api_profile');
   Route::get('news', 'ApiController@news')->name('api_news');
   Route::get('news/categories/{category}', 'ApiController@newsByCategory')->name('api_news_category');
   Route::get('news/{news}', 'ApiController@oneNews')->name('api_one_news');
   Route::get('subjects', 'ApiController@subjects')->name('api_subject');
   Route::get('subjects/{subject}', 'ApiController@oneSubject')->name('api_one_subject');
   Route::get('subjects/all/posts', 'ApiController@subjectPosts')->name('api_subject_posts');
   Route::get('subjects/{subject}/posts', 'ApiController@subjectPost')->name('api_per_subject_posts');
   Route::get('dm/{id}', 'ApiController@directMessages')->name('direct_message');
   Route::get('dm2/{id}', 'ApiController@directMessages2')->name('direct_message2');
   Route::get('dm3/{id}', 'ApiController@directMessages3')->name('direct_message3');
   Route::get('dm3/{logged_in_profile_id}/{profile_id}', 'ApiController@directMessagesConversation')->name('direct_message_conversation');
   Route::get('forum', 'ApiController@forum')->name('api_forum');
   Route::get('association', 'ApiController@association')->name('api_association');
   Route::get('faculties', 'ApiController@getFaculties')->name('api_get_faculties');
   Route::get('courses', 'ApiController@getCourses')->name('api_get_courses');
   Route::get('roles', 'ApiController@getRoles')->name('api_get_roles');
   Route::get('search{name?}', 'ApiController@getSearchResults')->name('api_get_search_results');

   //allow data to be posted
   Route::post('auth/login', 'ApiController@login')->name('api_login');
   Route::post('dm/send', 'ApiController@dmSend')->name('api_dm_send');
   Route::get('/comment/news/{news}', 'ApiController@getNewsComment')->name('api_get_news_comment');
   Route::post('/comment/news/{news}', 'ApiController@postNewsComment')->name('api_post_news_comment');
   Route::get('/forums', 'ApiController@getForums')->name('api_get_forums');
   Route::post('/forum', 'ApiController@postForum')->name('api_post_forum');
    Route::get('/comment/forums/{forum}', 'ApiController@getForumComment')->name('api_get_forum_comment');
    Route::post('/comment/forums/{forum}', 'ApiController@postForumComment')->name('api_post_forum_comment');

});
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
