<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NewMedia extends Model
{
    //mass assignment
    protected $fillable = ['new_id', 'media_type_id', 'media'];

    public function news() {
        return $this->belongsTo('App\News');
    }
}
