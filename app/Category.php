<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    //mass assignment
    protected $fillable = ['name'];

    public function news() {
        return $this->belongsToMany('App\News', 'new_category', 'category_id', 'new_id');
    }
}
