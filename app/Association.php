<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Association extends Model
{
    //mass assignment
    protected $fillable = ['profile_id', 'name', 'avatar', 'phone', 'email', 'website', 'address', 'description'];

    public function profile() {
        return $this->belongsTo('App\Profile');
    }
}
