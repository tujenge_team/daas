<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubjectCategory extends Model
{
    //mass assignment
    protected $fillable = ['name'];

    public function posts() {
        return $this->hasMany('App\SubjectPost');
    }
}
