<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Faculty extends Model
{
    protected $fillable = ['name', 'university_id'];

    protected function university() {
        return $this->belongsTo('\App\University');
    }
    public function courses() {
        return $this->hasMany('App\Course');
    }

    public function profiles() {
        return $this->belongsToMany('App\Profile', 'profile_faculties');
    }
}
