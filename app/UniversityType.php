<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UniversityType extends Model
{
    //mass assignment
    protected $fillable = ['name'];

    public function universities() {
        return $this->hasMany('\App\University');
    }
}
