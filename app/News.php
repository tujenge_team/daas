<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class News extends Model
{
    //mass assignment
    protected $fillable = ['title', 'media'];

    public function profile() {
        return $this->belongsTo('App\Profile');
    }

    public function medias() {
        return $this->hasMany('App\NewMedia', 'new_id');
    }

    public function categories() {
        return $this->belongsToMany('App\Category', 'new_category', 'new_id');
    }

    public function comments() {
        return $this->belongsToMany('App\Comment', 'comment_new', 'new_id', 'comment_id');
    }
}
