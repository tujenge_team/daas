<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
//    mass assignemt
    protected $fillable = ['profile_id', 'receiver_id', 'message', 'read', 'reply'];

    public function profile() {
        return $this->belongsTo('App\Profile');
    }

    public function media() {
        return $this->hasOne('App\MessageMedia');
    }
}
