<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProfileContact extends Model
{
    protected $fillable = ['profile_id','email', 'phone', 'address'];

    public function profile() {
        return $this->belongsTo('App\Profile');
    }
}
