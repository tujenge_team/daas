<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ResourceMedia extends Model
{
    //mass assignment
    protected $fillable = ['resource_id', 'media_type_id', 'media'];

    public function resource() {
        return $this->belongsTo('App\Resource');
    }
}
