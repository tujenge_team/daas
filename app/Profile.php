<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    //mass assignment
    protected $fillable = ['user_id', 'first_name', 'middle_name', 'last_name', 'identifier', 'avatar', 'registration_year'];

    public function user() {
        return $this->belongsTo('App\User');
    }

    public function subjects() {
        return $this->belongsToMany('App\Subject', 'profile_subject');
    }

    public function courses() {
        return $this->belongsToMany('App\Course', 'profile_course');
    }

    public function faculties() {
        return $this->belongsToMany('App\Faculty', 'profile_faculties');
    }

    public function news() {
        return $this->hasOne('App\News');
    }

    public function comments() {
        return $this->hasMany('App\Comment');
    }

    public function messagesReceived() {
        return $this->hasMany('App\Message', 'receiver_id');
    }

    public function messagesSent() {
        return $this->hasMany('App\Message', 'profile_id');
    }

    public function association() {
        return $this->hasOne('App\Association');
    }

    public function contact() {
        return $this->hasOne('App\ProfileContact');
    }
    public function forums() {
        return $this->hasMany('App\Forum');
    }
}
