<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Forum extends Model
{
    //mass assignment
    protected $fillable = ['profile_id', 'name', 'forum_type_id', 'registration_year'];

    public function type() {
        return $this->belongsTo('App\ForumType', 'forum_type_id');
    }

    public function profile() {
        return $this->belongsTo('App\Profile');
    }

    public function faculty() {
        return $this->hasOne('App\ForumType');
    }

    public function comments() {
        return $this->belongsToMany('App\Comment', 'comment_forum', 'forum_id', 'comment_id')->withPivot('forum_id');
    }
}
