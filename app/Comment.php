<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    //mass assignment
    protected $fillable = ['profile_id', 'comment'];

    public function news() {
        return $this->belongsToMany('App\New', 'comment_new', 'new_id', 'comment_id');
    }

    public function forum() {
        return $this->belongsToMany('App\Forum', 'comment_forum', 'comment_id', 'forum_id');
    }

    public function profile() {
        return  $this->belongsTo('App\Profile');
    }
}
