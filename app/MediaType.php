<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MediaType extends Model
{
    //mass assignment
    protected $fillable = ['type'];
}
