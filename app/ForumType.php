<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ForumType extends Model
{
    //mass assignment
    protected $fillable = ['type'];

    public function forum() {
        return $this->hasOne('App\Forum');
    }
}
