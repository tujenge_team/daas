<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Semester extends Model
{
    //mass assignment
    protected $fillable = ['name', 'university_id', 'start_date', 'end_date'];

    public function university() {
        return $this->belongsTo('App\University');
    }
}
