<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    protected $fillable = ['name', 'faculty_id', 'duration'];

    public function faculty() {
        return $this->belongsTo('App\Faculty');
    }
    
    public function subjects() {
        return $this->hasMany('App\Subject');
    }

    public function profiles() {
        return $this->belongsToMany('App\Profile', 'profile_course');
    }
}
