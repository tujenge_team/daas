<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subject extends Model
{
//    mass assignment
    protected $fillable = ['name', 'course_id'];

    public function course() {
        return $this->belongsTo('App\Course');
    }
    public function profiles() {
        return $this->belongsToMany('App\Profile', 'profile_subject');
    }

    public function post() {
        return $this->hasMany('App\SubjectPost');
    }
    // public function category() {
    //     return $this->hasManyThrough('App\SubjectCategory', 'App\SubjectPost', 'subject_id', 'subject_category_id');
    // }
}
