<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubjectPost extends Model
{
    //mass assignment
    protected $fillable = ['subject_id','subject_category_id', 'title', 'content'];

    public function subject(){
        return $this->belongsTo('App\Subject');
    }

    public function category() {
        return $this->belongsTo('App\SubjectCategory', 'subject_category_id');
    }
}
