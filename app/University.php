<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class University extends Model
{
    protected $fillable = ['name', 'university_type_id', 'registration', 'location'];

    public function universityType() {
        return $this->belongsTo('\App\UniversityType');
    }
    
    public  function faculties() {
        return $this->hasMany('\App\Faculty');
    }

    public function semesters() {
        return $this->hasMany('App\Semester');
    }

    public function users() {
        return $this->hasMany('App\User');
    }
}
