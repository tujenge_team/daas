<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Resource extends Model
{
    //mass assignment
    protected $fillable = ['title', 'content'];

    public function medias() {
        return $this->hasMany('App\ResourceMedia');
    }
}
