<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MessageMedia extends Model
{
    //mass assignment
    protected $fillable = ['message_id', 'media', 'media_type_id'];

    public function message() {
        return $this->belongsTo('App\Message');
    }
}
