<?php

namespace App\Http\Controllers;

use App\News;
use Illuminate\Http\File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class NewController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $news = News::all();
        return view('test.news', compact('news'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('forms.news');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(!Auth::user()) {
            return redirect('/login');
        }
        if($request->file) {
            $file = Storage::putFile('public/news', new File($request->file));
        }
        else {
            $file = '';
        }
        $news = new News;
        $news->profile_id = $request->user()->profile->id;
        $news->title = $request->input('title');
        $news->content = $request->input('content');
        $news->save();

        $news->medias()->create([
            'media_type_id' => $request->media_type_id,
            'media' => $file
        ]);

        $news->categories()->attach($request->category_id);

        return $this->index();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $news = News::find($id);
        return $news;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $news = News::find($id);
        return view('forms.news', compact('news'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if(!Auth::user()) {
            return redirect('/login');
        }
        if($request->file) {
            $file = Storage::putFile('public/news', new File($request->file));
        }
        else {
            $file = '';
        }
        $news = News::find($id);
        $news->profile_id = $request->user()->profile->id;
        $news->title = $request->input('title');
        $news->content = $request->input('content');
        $news->save();

        $news->medias()->create([
            'media_type_id' => $request->media_type_id,
            'media' => $file
        ]);

        $news->categories()->attach($request->category_id);
        return $this->index();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $news = News::find($id);
        $news->delete();
        return $this->index();
    }
}
