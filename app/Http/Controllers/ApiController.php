<?php

namespace App\Http\Controllers;

use App\Association;
use App\Category;
use App\Course;
use App\Faculty;
use App\Forum;
use App\Message;
use App\News;
use App\Profile;
use App\Role;
use App\Subject;
use App\SubjectPost;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;

class ApiController extends Controller
{
    use AuthenticatesUsers;
//    get profiles
    public  function profiles() {
        $profiles = Profile::with('user.roles','contact', 'courses', 'faculties')->orderBy('created_at', 'desc')->get();
        return response()->json(['profiles' => $profiles]);
    }
//    get user
    public  function profile($id) {
        $profile = Profile::with(['user','courses','subjects', 'forums'])->find($id);
        return response()->json(['profile' => [$profile]]);
    }
    //get news
    public function news() {
        $news = News::with(['medias', 'profile', 'categories'])->get();
        return response()->json(['news' => $news]);
    }

    //get news by category
    public function newsByCategory($id) {
        $category = Category::with('news')->find($id);
        return response()->json(['categories' => $category]);
    }

    //get one/single news
    public function oneNews($id) {
        $news = News::with(['medias', 'profile','comments.profile'])->find($id);
        return response()->json([$news]);
    }

//    get subjects
    public function subjects() {
        $subject= Subject::with('course')->get();
        return response()->json(['subjects' => $subject]);
    }

    //get one/single subject
    public function oneSubject($id) {
        $subject = Subject::with(['course'])->find($id);
        return response()->json(['subject' => $subject]);
    }

    //get one/single one subject post
    public function oneSubjectPost($id) {
        $oneSubjectPost = SubjectPost::with('category')->find($id);
        return response()->json(['subjectPost' => $oneSubjectPost]);
    }
    //get  subject posts
    public function subjectPost($id) {
        $posts = Subject::findorfail($id)->post()->with('category')->get();
        return response()->json(['posts' => $posts]);
    }

    //get subject posts
    public function subjectPosts() {
        $subjectPosts = SubjectPost::with('category')->get();
        return response()->json(['subjectPost' => $subjectPosts]);
    }

    //get forums
    public function forum() {
        $forums = Forum::with('profile','type', 'comments.profile')->get();
        return response()->json(['forums' => $forums]);
    }

    //get associations
    public function association() {
        $associations = Association::with('profile')->get();
        return response()->json(['associations' => $associations]);
    }

    //get dms
    public function directMessages($id) {
        $sent = Profile::find($id)->messagesSent;
        $received = Profile::find($id)->messagesReceived;
        return response()->json(['sent' => $sent,
            'received' => $received]);
    }

    //get dms
    public function directMessages2($id) {
        $dm = Profile::with('messagesSent.profile', 'messagesReceived.profile')->find($id);
        return response()->json(['messages' => $dm]);
    }

    //get dms
    public function directMessages3($id) {
        $dm = Profile::with(['messagesReceived' => function ($q) {
            $q->with('profile')->groupBy('profile_id');
        }])->find($id);
//        $dm = Profile::with('messagesSent.profile', 'messagesReceived.profile')->find($id);

        return response()->json(['messages' => $dm]);
    }

    //get dm convo
    public function directMessagesConversation($prof_id, $id) {
        $convo = Message::where(['receiver_id' => $prof_id, 'profile_id' => $id])->orWhere(['receiver_id' => $id, 'profile_id' => $prof_id])->get();
        return response()->json(['conversation' => $convo]);
    }

//    get faculties
    public function getFaculties() {
        $faculties= Faculty::all();
        return response()->json(['faculties' => $faculties]);
    }

//    get courses
    public function getCourses() {
        $courses= Course::all();
        return response()->json(['courses' => $courses]);
    }

//    get subjects
    public function getSubjects() {
        $subject = Subject::with('course')->get();
        return response()->json(['subjects' => $subject]);
    }

//    get roles
    public function getRoles() {
        $roles = Role::all();
        return response()->json(['roles' => $roles]);
    }

    //login
    public function login(Request $request){
        if(Auth::attempt(['email' => $request->email, 'password' => $request->password],['remember' => true])) {
            $user = User::with(['profile' => function ($q) {
                $q->with('contact', 'courses', 'faculties');
            }])->find($request->user()->id);
            return response()->json(['user' => $user]);
        }
        return "failed";
    }

    //send dm
    public function dmSend(Request $request) {
        //check if user is authenticated
//        if(!Auth::user()){
//            return "You have to be logged in";
//        }

        $dm = new Message;
        $dm->profile_id = $request->profile_id;
        $dm->receiver_id = $request->receiver_id;
        $dm->message = $request->message;
        $dm->reply = true;
        $dm->save();

        if($request->hasFile('media')) {
            $dm->media()->create([
                'media' => $request->media->store('chats'),
                'media_type_id'=> $request->media_type_id
            ]);
        }

        return response()->json($dm);
    }

//    get news comments
    public function getNewsComment($id) {
        $comments = News::with('comments.profile')->find($id);
        return response()->json(['comments'=>$comments]);

    }

    //post news comment
    public function postNewsComment(Request $request, $id) {
        $news = News::findorfail($id);
        $news->comments()->create($request->input());
        return response()->json(['comment' => $request->input(), 'message' => 'All good'    ]);
    }

//    get forums
    public function getForums() {
        $forum = Forum::with('profile')->get();
        return response()->json(['forums' => $forum]);
    }

//    post forum
    public function postForum(Request $request) {
        $forum = Forum::create($request->input());
        return response()->json(['message' => 'Forum Created', 'forum' => $forum]);
    }

    //post forum comment
    public function postForumComment(Request $request, $id) {
        $forum = Forum::findorfail($id);
        $forum->comments()->create($request->input());
        return response()->json(['comment' => $request->input(), 'message' => 'All good'    ]);
    }

//    get forum comments
    public function getForumComment($id) {
        $comments = Forum::with('comments.profile')->find($id);
        return response()->json(['comments'=>$comments]);

    }

//    get profiles from search
    public function getSearchResults(Request $request) {
        $profiles = Profile::where('first_name', 'like', "%{$request->name}%")->orWhere('middle_name', 'like', "%{$request->name}%")->orWhere('last_name', 'like', "%{$request->name}%")->get();
        return response()->json(['profiles'=>$profiles]);

    }
}

