<?php

namespace App\Http\Controllers;

use App\Course;
use App\Faculty;
use App\News;
use App\UniversityType;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class DashboardController extends Controller
{
    public function index(){
        return view('dashboard.index');
    }
    public function universitySetup(){
        $types = UniversityType::all();
        $faculties = Faculty::all();
        $courses = Course::all();
        return view('dashboard.university-setup')->with(['types' => $types, 'faculties' => $faculties, 'courses' => $courses]);
    }
    public function staffRegistration(){
        return view('dashboard.staff-registration');
    }
    public function studentRegistration(){
        return view('dashboard.student-registration');
    }
    public function staffboard(){
        $news = News::orderBy('created_at', 'DESC')->get();
        return view('dashboard.staffboard', compact('news'));
    }
    public function staffboardMessage(){
        $news = News::orderBy('created_at', 'DESC')->get();
        return view('dashboard.per-staffboard', compact('news'));
    }
    public function messages(Request $request){
        $messages = $request->user()->profile->messagesReceived()->orderBy('created_at', 'desc')->get();
        return view('dashboard.messages', compact('messages'));
    }
    public function message(Request $request, $id){
        $messages = $request->user()->profile->messagesReceived()->orderBy('created_at', 'desc')->groupBy('profile_id')->get();
        return view('dashboard.per-message', compact('messages'));
    }
    public function announcements(){
        return view('dashboard.forms.announcements');
    }
    public function notes(){
        return view('dashboard.forms.notes');
    }
    public function assignments(){
        return view('dashboard.forms.assignments');
    }
    public function courseWork(){
        return view('dashboard.forms.course-work');
    }
    public function autoSave(){
        return view('dashboard.auto-save');
    }
}
