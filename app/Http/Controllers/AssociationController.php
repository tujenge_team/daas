<?php

namespace App\Http\Controllers;

use App\Association;
use Illuminate\Http\File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class AssociationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $associations = Association::all();
        return view('test.association', compact('associations'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('forms.association');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $association = new Association;
        $association->profile_id = $request->profile_id;
        $association->name = $request->name;
        $association->phone = $request->phone;
        $association->email = $request->email;
        $association->website = $request->website;
        $association->address = $request->address;
        $association->description = $request->description;
        $association->avatar = Storage::putFile('association', new File($request->avatar), 'public');
        $association->save();

        return $this->index();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $association = Association::find($id);
        return $association;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $association = Association::find($id);
        return view('forms.association', compact('association'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $association = Association::find($id);
        $association->profile_id = $request->profile_id;
        $association->name = $request->name;
        $association->phone = $request->phone;
        $association->email = $request->email;
        $association->website = $request->website;
        $association->address = $request->address;
        $association->description = $request->description;
        if($request->file('avatar')) {
            $association->avatar = Storage::putFile('association', new File($request->avatar), 'public');
        }
        $association->save();

        return $this->index();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $association = Association::find($id);
        $association->delete();
        return $this->index();
    }
}
