<?php

namespace App\Http\Controllers;

use App\SubjectPost;
use Illuminate\Http\Request;

class SubjectPostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = SubjectPost::with('category', 'subject')->get();
//        return $posts;
        return view('test.subjectPost', compact('posts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('forms.subjectPost');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
//        SubjectPost::create($request->input());
        $subject = new SubjectPost;
        $subject->subject_id = $request->subject_id;
        $subject->subject_category_id = $request->subject_category_id;
        $subject->title = $request->title;
        $subject->content = $request->input('content');
        if($request->hasFile('media')) {
         $subject->media = $request->media->store('subject_posts');
        }
        $subject->save();

        return $this->index();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $post = SubjectPost::find($id);
        return $post;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $post = SubjectPost::find($id);
        return view('forms.subjectPost', compact('post'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $post = SubjectPost::find($id);
        $post->update($request->input());
        return $this->index();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post = SubjectPost::find($id);
        $post->delete();
        return $this->index();
    }
}
