<?php

namespace App\Http\Controllers;

use App\Profile;
use App\University;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Symfony\Component\HttpFoundation\File\File;

class ProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $profiles = Profile::all();
        return view('test.profile', compact('profiles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('forms.profile');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request->json()) {

            $user = new User;
            $user->email = $request->email;
            $user->university_id = University::first()->id;
            $user->password = bcrypt(123456);
            if($user->save()) {
                $profile = new Profile;
                $profile->user_id = $user->id;
                $profile->first_name = $request->first_name;
                $profile->middle_name = $request->middle_name;
                $profile->last_name = $request->last_name;
                $profile->identifier = $request->identifier;
                if($request->has('registration_year')) {
                    $profile->registration_year = $request->registration_year;
                } else {
                    $profile->registration_year = Carbon::today();
                }
                $profile->avatar = $request->avatar->store('avatars');
                $profile->save();

                if($request->has('role_id')) {
                    $user->roles()->attach([
                        'role_id' => $request->role_id
                    ]);
                }

                $profile->faculties()->attach([
                    'faculty_id' => $request->faculty_id
                ]);

                $profile->courses()->attach([
                    'course_id' => $request->course_id
                ]);

                if($request->has(['phone', 'email', 'address'])) {
                    $profile->contact()->create([
                        'email' => $request->email,
                        'phone' => $request->phone,
                        'address' => $request->address
                    ]);
                }
            }

            return response()->json(['message' => 'saved']);
        } else {
            $user = $request->user()->id;
            $profile = new Profile;
            $profile->user_id = $user;
            $profile->first_name = $request->first_name;
            $profile->middle_name = $request->middle_name;
            $profile->last_name = $request->last_name;
            $profile->identifier = $request->identifier;
            $profile->registration_year = $request->registration_year;
            $profile->avatar = $request->avatar->store('avatars');
            $profile->save();
            return $this->index();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $profile = Profile::find($id);
        return $profile;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $profile = Profile::find($id);
        return view('forms.profile', compact('profile'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

//        if($request->hasFile('avatar')) {
////            $file = \Storage::putFile('public/avatars', new File($request->avatar), 'public');
//            $file = "";
//        }
//        else {
//            $file = "";
//        }
        $profile = Profile::find($id);
        $profile->first_name = $request->first_name;
        $profile->middle_name = $request->middle_name;
        $profile->last_name = $request->last_name;
        $profile->identifier = $request->identifier;
        $profile->registration_year = $request->registration_year;
        $profile->avatar = Storage::putFile('avatars', new File($request->avatar));
        $profile->save();
        return $this->index();
//        $profile->update([
//            'first_name' => $request->first_name,
//            'middle_name' => $request->middle_name,
//            'last_name' => $request->last_name,
//            'identifier' => $request->identifier,
//            'registration_year' => $request->registration_year,
//            'avatar' => $file
//        ]);
//        return $this->index();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $profile = Profile::find($id);
        $profile->delete();
        return $this->index();
    }
}
