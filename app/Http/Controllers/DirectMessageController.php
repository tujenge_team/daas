<?php

namespace App\Http\Controllers;


use App\Message;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class DirectMessageController extends Controller
{
    public function sendMessage(Request $request) {
        //check if user is authenticated
        if(!Auth::user()){
            return "You have to be logged in";
        }

        $dm = new Message;
        $dm->profile_id = $request->user()->profile->id;
        $dm->receiver_id = $request->receiver_id;
        $dm->message = $request->message;
        $dm->reply = true;
        $dm->save();

        if($request->hasFile('media')) {
            $dm->media()->create([
                'media' => $request->media->store('chats'),
                'media_type_id'=> $request->media_type_id
            ]);
        }


        return $dm;
    }

    public function getConversation(Request $request, $id) {
        $convo = Message::where(['receiver_id' => $request->user()->profile->id, 'profile_id' => $id])->orWhere(['receiver_id' => $id, 'profile_id' => $request->user()->profile->id])->get();
        return response()->json($convo);
    }
}
