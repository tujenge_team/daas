<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMessageMediaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('message_media', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('media_type_id')->unsigned()->nullable();
            $table->integer('message_id')->unsigned();
            $table->string('media');
            $table->timestamps();

            $table->foreign('media_type_id')->references('id')->on('media_types')->onDelete('set null');
            $table->foreign('message_id')->references('id')->on('messages');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('message_media');
    }
}
