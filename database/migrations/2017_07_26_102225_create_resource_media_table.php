<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateResourceMediaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('resource_media', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('media_type_id')->unsigned();
            $table->integer('resource_id')->unsigned();
            $table->string('media');
            $table->timestamps();

            $table->foreign('media_type_id')->references('id')->on('media_types');
            $table->foreign('resource_id')->references('id')->on('resources');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('resource_media');
    }
}
