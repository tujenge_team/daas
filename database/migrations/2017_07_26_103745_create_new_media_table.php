<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNewMediaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('new_media', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('media_type_id')->unsigned();
            $table->integer('new_id')->unsigned();
            $table->string('media');
            $table->timestamps();

            $table->foreign('media_type_id')->references('id')->on('media_types');
            $table->foreign('new_id')->references('id')->on('news');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('new_media');
    }
}
