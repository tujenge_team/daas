<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateForumsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('forums', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('profile_id')->unsigned()->nullable();
            $table->integer('forum_type_id')->unsigned();
            $table->string('name');
            $table->time('registration_year');
            $table->timestamps();

            $table->foreign('forum_type_id')
                ->references('id')
                ->on('forum_types')
                ->onDelete('cascade');

            $table->foreign('profile_id')
                ->references('id')->on('profiles')
                ->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('forums');
    }
}
