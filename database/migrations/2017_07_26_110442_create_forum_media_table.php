<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateForumMediaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('forum_media', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('media_type_id')->unsigned();
            $table->integer('forum_id')->unsigned();
            $table->string('media');
            $table->timestamps();

            $table->foreign('media_type_id')->references('id')->on('media_types');
            $table->foreign('forum_id')->references('id')->on('forums');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('forum_media');
    }
}
