<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('categories')->insert([
            'name' => 'announcement'
        ]);

        DB::table('categories')->insert([
            'name' => 'course-work'
        ]);

        DB::table('categories')->insert([
            'name' => 'assignment'
        ]);

        DB::table('categories')->insert([
            'name' => 'notes'
        ]);
    }
}
