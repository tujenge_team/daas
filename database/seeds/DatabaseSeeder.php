<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
//         $this->call(UsersTableSeeder::class);
//        factory(App\UniversityType::class, 3)->create()->each(function ($type) {
//           $type->universities()->save(factory(App\University::class)->make());
//        });
//
//        factory(App\User::class, 5)->create()->each(function ($u) {
//            $u->profile()->save(factory(App\Profile::class)->create()->each(function ($profile) {
//                $profile->messagesSent()->save(factory(App\Message::class, 10)->make());
//            }));
//        });

        DB::table('categories')->insert([
            'name' => 'announcement'
        ]);

        DB::table('categories')->insert([
            'name' => 'course-work'
        ]);

        DB::table('categories')->insert([
            'name' => 'assignment'
        ]);

        DB::table('categories')->insert([
            'name' => 'notes'
        ]);

//        $this->call(RoleTableSeeder::class);
//        $this->call(CategoryTableSeeder::class);

        factory(App\UniversityType::class, 1)->create()->each(function ($u) {
            $u->universities()->save(factory(App\University::class)->make());
        });
        factory(App\User::class, 3)->create()->each(function ($profile) {
            $profile->profile()->save(factory(App\Profile::class)->create()->each(function ($contact){
                $contact->forums()->save(factory(App\Forum::class)->make());
                $contact->contact()->save(factory(App\ProfileContact::class)->make());
            }));
        });


    }
}
