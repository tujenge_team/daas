<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\UniversityType::class, function (Faker\Generator $faker) {
   return [
       'name' => $faker->title
   ];
});


$factory->define(App\University::class, function (Faker\Generator $faker) {
   return [
       'university_type_id' => function() { return factory(App\UniversityType::class)->create()->id; },
       'name' => $faker->streetName
   ];
});


$factory->define(App\User::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'university_id' => function() { return factory(App\University::class)->create()->id; },
        'email' => $faker->unique()->safeEmail,
        'password' => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
    ];
});

$factory->define(App\Profile::class, function( Faker\Generator $faker) {
   return [
     'user_id' => function() { return factory(App\User::class)->create()->id; },
     'first_name' => $faker->firstName,
     'middle_name' => $faker->lastName,
     'last_name' => $faker->lastName,
     'identifier' => $faker->randomDigit,
     'avatar' => $faker->imageUrl(),
     'registration_year' => $faker->dateTime
   ];
});

$factory->define(App\ForumType::class, function (Faker\Generator $faker) {
   return [
       'type' => $faker->chrome
   ] ;
});

$factory->define(App\Forum::class, function( Faker\Generator $faker) {
   return [
     'profile_id' => function() { return factory(App\Profile::class)->create()->id; },
     'forum_type_id' => function() { return factory(App\ForumType::class)->create()->id; },
     'name' => $faker->sentence,
     'registration_year' => $faker->year
   ];
});


$factory->define(App\ProfileContact::class, function( Faker\Generator $faker) {
   return [
     'profile_id' => function() { return factory(App\Profile::class)->create()->id; },
     'phone' => $faker->phoneNumber,
     'email' => $faker->email
   ];
});


$factory->define(App\Message::class, function(Faker\Generator $faker) {
    return [
        'profile_id' => function() { return factory(App\Profile::class)->create()->id; },
        'receiver_id' => function() { return factory(App\Profile::class)->create()->id; },
        'message' => $faker->paragraph,
        'read' => $faker->boolean,
        'reply' => $faker->boolean
    ];
});