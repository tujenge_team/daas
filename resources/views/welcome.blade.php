<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>DAAS</title>

        {{--css--}}
        <link rel="stylesheet" href="{{ asset('css/bulma.css') }}">

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

        {{--icons--}}
        <script src="https://use.fontawesome.com/cbb1fa18b0.js"></script>
        {{--<meta charset="utf-8">--}}
        {{--<meta http-equiv="X-UA-Compatible" content="IE=edge">--}}
        {{--<meta name="viewport" content="width=device-width, initial-scale=1">--}}

        {{--<title>DAAS</title>--}}

        <!-- Fonts -->
        {{--<link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">--}}

        <!-- Styles -->
        {{--<style>--}}
            {{--html, body {--}}
                {{--background-color: #fff;--}}
                {{--color: #636b6f;--}}
                {{--font-family: 'Raleway', sans-serif;--}}
                {{--font-weight: 100;--}}
                {{--height: 100vh;--}}
                {{--margin: 0;--}}
            {{--}--}}

            {{--.full-height {--}}
                {{--height: 100vh;--}}
            {{--}--}}

            {{--.flex-center {--}}
                {{--align-items: center;--}}
                {{--display: flex;--}}
                {{--justify-content: center;--}}
            {{--}--}}

            {{--.position-ref {--}}
                {{--position: relative;--}}
            {{--}--}}

            {{--.top-right {--}}
                {{--position: absolute;--}}
                {{--right: 10px;--}}
                {{--top: 18px;--}}
            {{--}--}}

            {{--.content {--}}
                {{--text-align: center;--}}
            {{--}--}}

            {{--.title {--}}
                {{--font-size: 84px;--}}
            {{--}--}}

            {{--.links > a {--}}
                {{--color: #636b6f;--}}
                {{--padding: 0 25px;--}}
                {{--font-size: 12px;--}}
                {{--font-weight: 600;--}}
                {{--letter-spacing: .1rem;--}}
                {{--text-decoration: none;--}}
                {{--text-transform: uppercase;--}}
            {{--}--}}

            {{--.m-b-md {--}}
                {{--margin-bottom: 30px;--}}
            {{--}--}}
        {{--</style>--}}
    </head>
    <body>
    <div class="hero">
        <div class="hero-body">
            <div class="container">
                <div class="columns">
                    <div class="is-3">
                        <aside class="menu">
                            <ul class="menu-list">
                                <li>
                                    <a class="is-active">University Type</a>
                                    <ul>
                                        <li><a href="{{ route('university-type.index') }}">Types</a></li>
                                    </ul>
                                </li>
                                <li>
                                    <a class="is-active">University</a>
                                    <ul>
                                        <li><a href="{{ route('university.index') }}">University</a></li>
                                    </ul>
                                </li>
                                <li>
                                    <a class="is-active">Faculty</a>
                                    <ul>
                                        <li><a href="{{ route('faculty.index') }}">Faculty</a></li>
                                    </ul>
                                </li>
                                <li>
                                    <a class="is-active">Course</a>
                                    <ul>
                                        <li><a href="{{ route('course.index') }}">Course</a></li>
                                    </ul>
                                </li>
                                <li>
                                    <a class="is-active">Subject</a>
                                    <ul>
                                        <li><a href="{{ route('subject.index') }}">Subject</a></li>
                                        <li><a href="{{ route('subject-category.index') }}">Subject Categories</a></li>
                                        <li><a href="{{ route('subject-post.index') }}">Post</a></li>
                                    </ul>
                                </li>
                                <li>
                                    <a class="is-active">Semester</a>
                                    <ul>
                                        <li><a href="{{ route('semester.index') }}">Semester</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </aside>
                    </div>
                    <div class="is-3">
                        <aside class="menu is-paddingless">
                            <ul class="menu-list">
                                <li>
                                    <a class="is-active">Roles</a>
                                    <ul>
                                        <li><a href="{{ route('role.index') }}">Roles</a></li>
                                    </ul>
                                </li>
                                <li>
                                    <a class="is-active">Users</a>
                                    <ul>
                                        <li><a href="{{ route('user.index') }}">Users</a></li>
                                    </ul>
                                </li>
                                <li>
                                    <a class="is-active">User Roles</a>
                                    <ul>
                                        <li><a href="{{ route('role_user.index') }}">User Roles</a></li>
                                    </ul>
                                </li>
                                <li>
                                    <a class="is-active">Profile</a>
                                    <ul>
                                        <li><a href="{{ route('profile.index') }}">Profile</a></li>
                                    </ul>
                                </li>
                                <li>
                                    <a class="is-active">Profile Subjects</a>
                                    <ul>
                                        <li><a href="{{ route('profile_subject.index') }}">Profile Subjects</a></li>
                                    </ul>
                                </li>
                                <li>
                                    <a class="is-active">Course Profile</a>
                                    <ul>
                                        <li><a href="{{ route('course_profile.index') }}">Course Profile</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </aside>
                    </div>
                    <div class="is-3">
                        <aside class="menu is-paddingless">
                            <ul class="menu-list">
                                <li>
                                    <a class="is-active">Category</a>
                                    <ul>
                                        <li><a href="{{ route('category.index') }}">Category</a></li>
                                    </ul>
                                </li>
                                <li>
                                    <a class="is-active">Media Type</a>
                                    <ul>
                                        <li><a href="{{ route('media_type.index') }}">Media Type</a></li>
                                    </ul>
                                </li>
                                <li>
                                    <a class="is-active">Forum Type</a>
                                    <ul>
                                        <li><a href="{{ route('forum_type.index') }}">Forum Type</a></li>
                                    </ul>
                                </li>
                                <li>
                                    <a class="is-active">Resource</a>
                                    <ul>
                                        <li><a href="{{  route('resource.index') }}">Resource</a></li>
                                    </ul>
                                </li>
                                <li>
                                    <a class="is-active">News</a>
                                    <ul>
                                        <li><a href="{{ route('news.index') }}">News</a></li>
                                    </ul>
                                </li>
                                <li>
                                    <a class="is-active">Forums</a>
                                    <ul>
                                        <li><a href="{{ route('forum.index') }}">Forums</a></li>
                                    </ul>
                                </li>
                                <li>
                                    <a class="is-active">Association</a>
                                    <ul>
                                        <li><a href="{{ route('association.index') }}">Associations</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </aside>
                    </div>
                    <div class="is-3">
                        <aside class="menu is-paddingless">
                            <ul class="menu-list">
                                <li>
                                    <a class="is-active">API(s)</a>
                                    <ul>
                                        <li><a href="{{ route('api_profiles') }}">Profiles</a></li>
                                        <li><a href="{{ route('api_profile', 13) }}">Profile (prefilled with id "13")</a></li>
                                        <li><a href="{{ route('api_news') }}">News</a></li>
                                        <li><a href="{{ route('api_news_category', 1) }}">News By category</a></li>
                                        <li><a href="{{ route('api_one_news',1) }}">Single News (id "1" prefilled)</a></li>
                                        <li><a href="{{ route('api_subject') }}">Subjects</a></li>
                                        <li><a href="{{ route('api_one_subject',1) }}">Single Subject (id "1" prefilled)</a></li>
                                        <li><a href="{{ route('api_subject_posts') }}">Posts based on subjects</a></li>
                                        <li><a href="{{ route('api_per_subject_posts',1) }}">Posts per subject (id "1" prefilled)</a></li>
                                        <li><a href="{{ route('direct_message',14) }}">Direct Messages (id "14" prefilled)</a></li>
                                        <li><a href="{{ route('direct_message2',14) }}">Direct Messages {Angalia Hii then tell me what you think} (id "14" prefilled)</a></li>
                                        <li><a href="{{ route('direct_message3',14) }}">Direct Messages {Angalia Na Hii pia then tell me what you think} (id "14" prefilled)</a></li>
                                        <li><a href="{{ route('api_forum') }}">Forums</a></li>
                                        <li><a href="{{ route('api_association') }}">Associations</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </aside>
                    </div>
                    <div class="is-3">
                        <aside class="menu is-paddingless">
                            <ul class="menu-list">
                                <li>
                                    <a class="is-active">POSTING API(s)</a>
                                    <ul>
                                        <li><a href="{{ route('api_login') }}">Login URL: <code>{{ route('api_login') }}</code></a></li>
                                        <li><a href="{{ route('api_dm_send') }}">DM URL: <code>{{ route('api_dm_send') }}</code></a> PARAMS: <code>profile_id</code>, <code>receiver_id</code>, <code>message</code>, <code>media(has to be a file)</code>, <code>media_type_id</code> <strong>btw reply is always true</strong></li>
                                        <li><a href="{{ route('api_get_news_comment', 1) }}">GET NEWS COMMENT: <code>{{ route('api_get_news_comment', 1) }}</code></a> <code>ID is mutable</code></li>
                                        <li><a href="{{ route('api_post_news_comment', 1) }}">NEWS COMMENT: <code>{{ route('api_post_news_comment', 1) }} the id is mutable</code></a>
                                            PARAMS: <code>profile_id</code>, <code>comment</code></li>
                                        <li><a href="{{ route('api_get_forums') }}">GET FORUMS<code>{{ route('api_get_forums') }}</code></a></li>
                                        <li><a href="{{ route('api_post_forum') }}">CREATE FORUM: <code>{{ route('api_post_forum') }}</code></a>
                                            PARAMS: <code>name</code>, <code>registration_year</code>, <code>forum_type_id</code>
                                        </li>
                                        <li><a href="{{ route('api_get_forum_comment', 1) }}">GET FORUMS COMMENT: <code>{{ route('api_get_forum_comment', 1) }}</code></a> <code>ID is mutable</code></li>
                                        <li><a href="{{ route('api_post_forum_comment', 1) }}">FORUM COMMENT: <code>{{ route('api_post_forum_comment', 1) }} the id is mutable</code></a>
                                            PARAMS: <code>profile_id</code>, <code>comment</code></li><li><a href="{{ route('api_get_search_results') }}">SEARCH : <code>{{ route('api_get_search_results') }}</code></a>
                                            PARAMS: <code>name</code></li>
                                        <li><a href="{{ route('direct_message_conversation',['logged_in_profile_id' => 13, 'profile_id' => 14]) }}">DM CONVERSATION: <code>{{ route('direct_message_conversation',['logged_in_profile_id' => 13, 'profile_id' => 14]) }} the ids are mutable</code>PARAMS: <code>name</code>, <code>logged_in_user_id</code>, <code>profile_id</code></a>
                                    </ul>
                                </li>
                            </ul>
                        </aside>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </body>
</html>
