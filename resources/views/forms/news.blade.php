@extends('layouts.container')

@section('content')
    @if(isset($news))
        {!! Form::model($news, ['route' => ['news.update', $news->id], 'method' => 'PUT', 'files' => true]) !!}
    @else
        {!! Form::open(['route' => 'news.store', 'files' => true]) !!}
    @endif
    <div class="container">
        <div class="field">
            {!! Form::label('Title') !!}
            <div class="control">
                {!! Form::text('title', null, ['class' => 'input' , 'placeholder' => 'Title']) !!}
            </div>
        </div>
        <div class="field">
            {!! Form::label('Content') !!}
            <div class="control">
                {!! Form::textarea('content', null, ['class' => 'input' , 'placeholder' => 'Content']) !!}
            </div>
        </div>
        <div class="field">
            {!! Form::label('Media Type') !!}
            <div class="select">
                {!! Form::select('media_type_id',\App\MediaType::all()->pluck('type', 'id')) !!}
            </div>
        </div>
        <div class="field">
            {!! Form::label('Category') !!}
            <div class="select">
                {!! Form::select('category_id',\App\Category::all()->pluck('name', 'id')) !!}
            </div>
        </div>
        <div class="field">
            {!! Form::label('File') !!}
            <div class="control">
                {!! Form::file('file',['class' => 'input']) !!}
            </div>
        </div>
        <div class="control">
            {!! Form::submit('save', ['class' => 'button is-primary']) !!}
        </div>
    </div>
    {!! Form::close() !!}

@endsection()