@extends('layouts.container')

@section('content')
    @if(isset($semester))
        {!! Form::model($semester, ['route' => ['semester.update', $semester->id], 'method' => 'PUT']) !!}
    @else
        {!! Form::open(['route' => 'semester.store']) !!}
    @endif
    <div class="container">
        <div class="field">
            {!! Form::label('Semester') !!}
            <div class="control">
                {!! Form::text('name', null, ['class' => 'input' , 'placeholder' => 'Semester']) !!}
            </div>
        </div>
        <div class="field">
            {{ Form::label('University') }}
            <div class="control">
                {!! Form::select('university_id', \App\University::all()->pluck('name', 'id')) !!}
            </div>
        </div>
        <div class="field">
            {{ Form::label('Start Date') }}
            <div class="control">
                {!! Form::date('start_date') !!}
            </div>
        </div>
        <div class="field">
            {{ Form::label('End Date') }}
            <div class="control">
                {!! Form::date('end_date') !!}
            </div>
        </div>
        <div class="control">
            {!! Form::submit('save', ['class' => 'button is-primary']) !!}
        </div>
    </div>
    {!! Form::close() !!}

@endsection()