@extends('layouts.container')

@section('content')
    @if(isset($university))
        {!! Form::model($university, ['route' => ['university.update', $university->id], 'method' => 'PUT']) !!}
    @else
        {!! Form::open(['route' => 'university.store']) !!}
    @endif
    <div class="container">
        <div class="field">
            {!! Form::label('Name') !!}
            <div class="control">
                {!! Form::text('name', null, ['class' => 'input' , 'placeholder' => 'University Name']) !!}
            </div>
        </div>



        <div class="field">
            {{ Form::label('University Type') }}
            <div class="control">
                {!! Form::select('university_type_id', \App\UniversityType::all()->pluck('name', 'id')) !!}
            </div>
        </div>
        <div class="control">
            {!! Form::submit('save', ['class' => 'button is-primary']) !!}
        </div>
    </div>
    {!! Form::close() !!}

@endsection()