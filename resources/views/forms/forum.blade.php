@extends('layouts.container')

@section('content')
    @if(isset($forum))
        {!! Form::model($forum, ['route' => ['forum.update', $forum->id], 'method' => 'PUT']) !!}
    @else
        {!! Form::open(['route' => 'forum.store']) !!}
    @endif
    <div class="container">
        <div class="field">
            {!! Form::label('Name') !!}
            <div class="control">
                {!! Form::text('name', null, ['class' => 'input' , 'placeholder' => 'Forum Title/Name']) !!}
            </div>
        </div>
        <div class="field">
            {!! Form::label('Registration Year') !!}
            <div class="control">
                {!! Form::date('registration_year', null, ['class' => 'input']) !!}
            </div>
        </div>
        <div class="field">
            {!! Form::label('Forum Type') !!}
            <div class="control">
                {!! Form::select('forum_type_id',\App\ForumType::all()->pluck('type', 'id') ,['class' => 'input']) !!}
            </div>
        </div>
        <div class="field">
            {!! Form::label('Faculty') !!}
            <div class="control">
                {!! Form::select('faculty_id',\App\Faculty::all()->pluck('name', 'id') ,['class' => 'input']) !!}
            </div>
        </div>
        <div class="field">
            {!! Form::label('Course') !!}
            <div class="control">
                {!! Form::select('course_id',\App\Course::all()->pluck('name', 'id') ,['class' => 'input']) !!}
            </div>
        </div>
        <div class="control">
            {!! Form::submit('save', ['class' => 'button is-primary']) !!}
        </div>
    </div>
    {!! Form::close() !!}

@endsection()