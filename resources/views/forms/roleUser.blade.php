@extends('layouts.container')

@section('content')
    @if(isset($roleUser))
        {!! Form::model($roleUser, ['route' => ['role_user.update', $roleUser->id], 'method' => 'PUT']) !!}
    @else
        {!! Form::open(['route' => 'role_user.store']) !!}
    @endif
    <div class="container">
        <div class="field">
            {{ Form::label('User') }}
            <div class="control">
                {!! Form::select('user_id', \App\User::all()->pluck('email', 'id')) !!}
            </div>
        </div>
        <div class="field">
            {{ Form::label('Role') }}
            <div class="control">
                {!! Form::select('role_id', \App\Role::all()->pluck('name', 'id')) !!}
            </div>
        </div>
        <div class="control">
            {!! Form::submit('save', ['class' => 'button is-primary']) !!}
        </div>
    </div>
    {!! Form::close() !!}

@endsection()