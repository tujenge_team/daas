@extends('layouts.container')

@section('content')
    @if(isset($user))
        {!! Form::model($user, ['route' => ['user.update', $user->id], 'method' => 'PUT']) !!}
    @else
        {!! Form::open(['route' => 'user.store']) !!}
    @endif
    <div class="container">
        <div class="field">
            {{ Form::label('University') }}
            <div class="control">
                {!! Form::select('university_id', \App\University::all()->pluck('name', 'id')) !!}
            </div>
        </div>
        <div class="field">
            {!! Form::label('Email') !!}
            <div class="control">
                {!! Form::email('email', null, ['class' => 'input' , 'placeholder' => 'email@domain.com']) !!}
            </div>
        </div>
        <div class="field">
            {!! Form::label('Password') !!}
            <div class="control">
                {!! Form::password('password', [ 'class' => 'input']) !!}
            </div>
        </div>
        <div class="control">
            {!! Form::submit('save', ['class' => 'button is-primary']) !!}
        </div>
    </div>
    {!! Form::close() !!}

@endsection()