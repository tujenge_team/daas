@extends('layouts.container')

@section('content')
    @if(isset($faculty))
        {!! Form::model($faculty, ['route' => ['faculty.update', $faculty->id], 'method' => 'PUT']) !!}
    @else
        {!! Form::open(['route' => 'faculty.store']) !!}
    @endif
    <div class="container">
        <div class="field">
            {!! Form::label('Faculty') !!}
            <div class="control">
                {!! Form::text('name', null, ['class' => 'input' , 'placeholder' => 'Faculty']) !!}
            </div>
        </div>



        <div class="field">
            {{ Form::label('University') }}
            <div class="control">
                {!! Form::select('university_id', \App\University::all()->pluck('name', 'id')) !!}
            </div>
        </div>
        <div class="control">
            {!! Form::submit('save', ['class' => 'button is-primary']) !!}
        </div>
    </div>
    {!! Form::close() !!}

@endsection()