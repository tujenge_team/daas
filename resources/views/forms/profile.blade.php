@extends('layouts.container')

@section('content')
    @if(isset($profile))
        {!! Form::model($profile, ['route' => ['profile.update', $profile->id], 'method' => 'PUT', 'files' => true]) !!}
    @else
        {!! Form::open(['route' => 'profile.store', 'files' => true]) !!}
    @endif
    <div class="container">
        <div class="field">
            {{ Form::label('User ID') }}
            <div class="control">
                {!! Form::select('user_id', \App\User::all()->pluck('email', 'id')) !!}
            </div>
        </div>
        <div class="field">
            {!! Form::label('First Name') !!}
            <div class="control">
                {!! Form::text('first_name', null, ['class' => 'input' , 'placeholder' => 'First Name']) !!}
            </div>
        </div>
        <div class="field">
            {!! Form::label('Middle Name') !!}
            <div class="control">
                {!! Form::text('middle_name', null, ['class' => 'input' , 'placeholder' => 'Middle Name']) !!}
            </div>
        </div>
        <div class="field">
            {!! Form::label('Last Name') !!}
            <div class="control">
                {!! Form::text('last_name', null, ['class' => 'input' , 'placeholder' => 'Last Name']) !!}
            </div>
        </div>
        <div class="field">
            {!! Form::label('Identifier') !!}
            <div class="control">
                {!! Form::text('identifier', null, ['class' => 'input' , 'placeholder' => 'Reg.NO | Employee ID']) !!}
            </div>
        </div>
        <div class="field">
            {!! Form::label('Profile Pic') !!}
            <div class="control">
                {!! Form::file('avatar', ['class' => 'input']) !!}
            </div>
        </div>
        <div class="field">
            {!! Form::label('Registration Year') !!}
            <div class="control">
                {!! Form::date('registration_year', null, ['class' => 'input']) !!}
            </div>
        </div>
        <div class="control">
            {!! Form::submit('save', ['class' => 'button is-primary']) !!}
        </div>
    </div>
    {!! Form::close() !!}

@endsection()