@extends('layouts.container')

@section('content')
    @if(isset($course))
        {!! Form::model($course, ['route' => ['course.update', $course->id], 'method' => 'PUT']) !!}
    @else
        {!! Form::open(['route' => 'course.store']) !!}
    @endif
    <div class="container">
        <div class="field">
            {!! Form::label('Course') !!}
            <div class="control">
                {!! Form::text('name', null, ['class' => 'input' , 'placeholder' => 'Faculty']) !!}
            </div>
        </div>
        <div class="field">
            {{ Form::label('Faculty') }}
            <div class="control">
                {!! Form::select('faculty_id', \App\Faculty::all()->pluck('name', 'id')) !!}
            </div>
        </div>
        <div class="field">
            {{ Form::label('Duration') }}
            <div class="control">
                {!! Form::select('duration', ['3_months' => '3 months', '6_months' => '6 months', '9_months' => '9 months', ]) !!}
            </div>
        </div>
        <div class="control">
            {!! Form::submit('save', ['class' => 'button is-primary']) !!}
        </div>
    </div>
    {!! Form::close() !!}

@endsection()