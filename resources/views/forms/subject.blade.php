@extends('layouts.container')

@section('content')
    @if(isset($subject))
        {!! Form::model($subject, ['route' => ['subject.update', $subject->id], 'method' => 'PUT']) !!}
    @else
        {!! Form::open(['route' => 'subject.store']) !!}
    @endif
    <div class="container">
        <div class="field">
            {!! Form::label('Subject') !!}
            <div class="control">
                {!! Form::text('name', null, ['class' => 'input' , 'placeholder' => 'Faculty']) !!}
            </div>
        </div>



        <div class="field">
            {{ Form::label('Course') }}
            <div class="control">
                {!! Form::select('course_id', \App\Course::all()->pluck('name', 'id')) !!}
            </div>
        </div>
        <div class="control">
            {!! Form::submit('save', ['class' => 'button is-primary']) !!}
        </div>
    </div>
    {!! Form::close() !!}

@endsection()