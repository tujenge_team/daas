@extends('layouts.container')

@section('content')
    @if(isset($type))
        {!! Form::model($type, ['route' => ['media_type.update', $type->id], 'method' => 'PUT']) !!}
    @else
        {!! Form::open(['route' => 'media_type.store']) !!}
    @endif
    <div class="container">
        <div class="field">
            {!! Form::label('Media Type') !!}
            <div class="control">
                {!! Form::text('type', null, ['class' => 'input' , 'placeholder' => 'Media Type']) !!}
            </div>
        </div>
        <div class="control">
            {!! Form::submit('save', ['class' => 'button is-primary']) !!}
        </div>
    </div>
    {!! Form::close() !!}

@endsection()