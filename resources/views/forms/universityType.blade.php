@extends('layouts.container')

@section('content')
    @if(isset($type))
        {!! Form::model($type, ['route' => ['university-type.update', $type->id], 'method' => 'PUT']) !!}
    @else
        {!! Form::open(['route' => 'university-type.store']) !!}
    @endif
    <div class="container">
        <div class="field">
            {!! Form::label('Type') !!}
            <div class="control">
                {!! Form::text('name', null, ['class' => 'input' , 'placeholder' => 'Type']) !!}
            </div>
        </div>
        <div class="control">
            {!! Form::submit('save', ['class' => 'button is-primary']) !!}
        </div>
    </div>
    {!! Form::close() !!}

@endsection()