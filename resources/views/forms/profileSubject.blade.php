@extends('layouts.container')

@section('content')
    @if(isset($profileSubject))
        {!! Form::model($profileSubject, ['route' => ['profile_subject.update', $profileSubject->id], 'method' => 'PUT']) !!}
    @else
        {!! Form::open(['route' => 'profile_subject.store']) !!}
    @endif
    <div class="container">
        <div class="field">
            {{ Form::label('Profile') }}
            <div class="control">
                {!! Form::select('profile_id', \App\Profile::all()->pluck('first_name', 'id')) !!}
            </div>
        </div>
        <div class="field">
            {{ Form::label('Subject') }}
            <div class="control">
                {!! Form::select('subject_id', \App\Subject::all()->pluck('name', 'id')) !!}
            </div>
        </div>
        <div class="control">
            {!! Form::submit('save', ['class' => 'button is-primary']) !!}
        </div>
    </div>
    {!! Form::close() !!}

@endsection()