@extends('layouts.container')

@section('content')
    @if(isset($role))
        {!! Form::model($role, ['route' => ['role.update', $role->id], 'method' => 'PUT']) !!}
    @else
        {!! Form::open(['route' => 'role.store']) !!}
    @endif
    <div class="container">
        <div class="field">
            {!! Form::label('Role') !!}
            <div class="control">
                {!! Form::text('name', null, ['class' => 'input' , 'placeholder' => 'Role']) !!}
            </div>
        </div>
        <div class="control">
            {!! Form::submit('save', ['class' => 'button is-primary']) !!}
        </div>
    </div>
    {!! Form::close() !!}

@endsection()