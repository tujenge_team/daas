@extends('layouts.container')

@section('content')
    @if(isset($type))
        {!! Form::model($type, ['route' => ['forum_type.update', $type->id], 'method' => 'PUT']) !!}
    @else
        {!! Form::open(['route' => 'forum_type.store']) !!}
    @endif
    <div class="container">
        <div class="field">
            {!! Form::label('Forum Type') !!}
            <div class="control">
                {!! Form::text('type', null, ['class' => 'input' , 'placeholder' => 'Forum Type']) !!}
            </div>
        </div>
        <div class="control">
            {!! Form::submit('save', ['class' => 'button is-primary']) !!}
        </div>
    </div>
    {!! Form::close() !!}

@endsection()