@extends('layouts.container')

@section('content')
    @if(isset($category))
        {!! Form::model($category, ['route' => ['category.update', $category->id], 'method' => 'PUT']) !!}
    @else
        {!! Form::open(['route' => 'category.store']) !!}
    @endif
    <div class="container">
        <div class="field">
            {!! Form::label('Category') !!}
            <div class="control">
                {!! Form::text('name', null, ['class' => 'input' , 'placeholder' => 'Category']) !!}
            </div>
        </div>
        <div class="control">
            {!! Form::submit('save', ['class' => 'button is-primary']) !!}
        </div>
    </div>
    {!! Form::close() !!}

@endsection()