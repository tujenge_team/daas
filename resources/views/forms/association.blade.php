@extends('layouts.container')

@section('content')
    @if(isset($association))
        {!! Form::model($association, ['route' => ['association.update', $association->id], 'method' => 'PUT', 'files' => true]) !!}
    @else
        {!! Form::open(['route' => 'association.store', 'files' => true]) !!}
    @endif
    <div class="container">
        <div class="field">
            {!! Form::label('Name') !!}
            <div class="control">
                {!! Form::select('profile_id', App\Profile::pluck('first_name', 'id'), ['class' => 'input' , 'placeholder' => 'Name']) !!}
            </div>
        </div>
        <div class="field">
            {!! Form::label('Association Name') !!}
            <div class="control">
                {!! Form::text('name', null, ['class' => 'input']) !!}
            </div>
        </div>
        <div class="field">
            {!! Form::label('LOGO') !!}
            <div class="control">
                {!! Form::file('avatar', ['class' => 'file']) !!}
            </div>
        </div>
        <div class="field">
            {!! Form::label('Phone') !!}
            <div class="control">
                {!! Form::text('phone',null ,['class' => 'input']) !!}
            </div>
        </div>
        <div class="field">
            {!! Form::label('Email') !!}
            <div class="control">
                {!! Form::email('email',null ,['class' => 'input']) !!}
            </div>
        </div>
        <div class="field">
            {!! Form::label('Website') !!}
            <div class="control">
                {!! Form::url('website',null ,['class' => 'input', 'placeholder' => 'http://www.domain.com']) !!}
            </div>
        </div>
        <div class="field">
            {!! Form::label('Address') !!}
            <div class="control">
                {!! Form::text('address',null,['class' => 'input']) !!}
            </div>
        </div>
        <div class="field">
            {!! Form::label('Description') !!}
            <div class="control">
                {!! Form::text('description',null,['class' => 'textarea']) !!}
            </div>
        </div>
        <div class="control">
            {!! Form::submit('save', ['class' => 'button is-primary']) !!}
        </div>
    </div>
    {!! Form::close() !!}

@endsection()