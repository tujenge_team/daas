@extends('layouts.container')

@section('content')
    @if(isset($resource))
        {!! Form::model($resource, ['route' => ['resource.update', $resource->id], 'method' => 'PUT', 'files' => true]) !!}
    @else
        {!! Form::open(['route' => 'resource.store', 'files' => true]) !!}
    @endif
    <div class="container">
        <div class="field">
            {!! Form::label('Title') !!}
            <div class="control">
                {!! Form::text('title', null, ['class' => 'input' , 'placeholder' => 'Title']) !!}
            </div>
        </div>
        <div class="field">
            {!! Form::label('Content') !!}
            <div class="control">
                {!! Form::textarea('content', null, ['class' => 'input' , 'placeholder' => 'Content']) !!}
            </div>
        </div>
        <div class="field">
            {!! Form::label('Media Type') !!}
            <div class="control">
                {!! Form::select('media_type_id',\App\MediaType::all()->pluck('type', 'id') ,['class' => 'input']) !!}
            </div>
        </div>
        <div class="field">
            {!! Form::label('File') !!}
            <div class="control">
                {!! Form::file('file',['class' => 'input']) !!}
            </div>
        </div>
        <div class="control">
            {!! Form::submit('save', ['class' => 'button is-primary']) !!}
        </div>
    </div>
    {!! Form::close() !!}

@endsection()