@extends('layouts.container')

@section('content')
    @if(isset($post))
        {!! Form::model($post, ['route' => ['subject-post.update', $post->id], 'method' => 'PUT', 'files' => true]) !!}
    @else
        {!! Form::open(['route' => 'subject-post.store', 'files' => true]) !!}
    @endif
    <div class="container">

        <div class="field">
            {{ Form::label('Subject') }}
            <div class="control">
                {!! Form::select('subject_id', \App\Subject::all()->pluck('name', 'id')) !!}
            </div>
        </div>

        <div class="field">
            {{ Form::label('Category') }}
            <div class="control">
                {!! Form::select('subject_category_id', \App\SubjectCategory::all()->pluck('name', 'id')) !!}
            </div>
        </div>
        <div class="field">
            {!! Form::label('Title') !!}
            <div class="control">
                {!! Form::text('title', null, ['class' => 'input' , 'placeholder' => 'Title']) !!}
            </div>
        </div>
        <div class="field">
            {!! Form::label('content') !!}
            <div class="control">
                {!! Form::text('content', null, ['class' => 'input' , 'placeholder' => 'Content']) !!}
            </div>
        </div>
        <div class="field">
            {!! Form::label('file') !!}
            <div class="control">
                {!! Form::file('media', ['class' => 'file']) !!}
            </div>
        </div>
        <div class="control">
            {!! Form::submit('save', ['class' => 'button is-primary']) !!}
        </div>
    </div>
    {!! Form::close() !!}

@endsection()