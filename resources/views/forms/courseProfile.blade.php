@extends('layouts.container')

@section('content')
    @if(isset($courseProfile))
        {!! Form::model($courseProfile, ['route' => ['course_profile.update', $courseProfile->id], 'method' => 'PUT']) !!}
    @else
        {!! Form::open(['route' => 'course_profile.store']) !!}
    @endif
    <div class="container">
        <div class="field">
            {{ Form::label('Profile') }}
            <div class="control">
                {!! Form::select('profile_id', \App\Profile::all()->pluck('first_name', 'id')) !!}
            </div>
        </div>
        <div class="field">
            {{ Form::label('Course') }}
            <div class="control">
                {!! Form::select('course_id', \App\Course::all()->pluck('name', 'id')) !!}
            </div>
        </div>
        <div class="control">
            {!! Form::submit('save', ['class' => 'button is-primary']) !!}
        </div>
    </div>
    {!! Form::close() !!}

@endsection()