@extends('layouts.dash')

@section('title', 'Messages')

@section('content')
    <div class="hero">
        <div class="hero-body is-paddingless">
            <div :class="{'message-container' : show}">
                @foreach($messages as $message)
                    @if($message->profile_id)
                        <div class="column is-12 custom-padding">
                            <div class="box is-link has-padding-vertical-half @if($message->read) new active-message @endif" @click="toggleChat({{ $message->profile_id }})">
                                <article class="media">
                                    <figure class="media-left">
                                        <p class="image is-64x64">
                                            <img class="img-circle" src="http://bulma.io/images/placeholders/128x128.png">
                                        </p>
                                    </figure>
                                    <div class="media-content">
                                        <div class="content">
                                            <p class="message-container-trunc truncate">
                                                <strong>{{ $message->profile->first_name }} {{ $message->profile->last_name }} </strong>
                                                <br>
                                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin ornare magna eros
                                            </p>
                                        </div>
                                    </div>
                                    <div class="media-right">
                                <span class="is-pulled-right">
                                {{ date_format(date_create($message->created_at), "d, M h:i") }}
{{--                                @php date('H:m', date_create($message->created_at)) @endphp--}}
                            </span>
                                    </div>
                                </article>
                            </div>
                        </div>
                        @continue
                    @endif
                @endforeach
            </div>
            <div v-if="show">
                <student-messages :profile_id="getProfileId"></student-messages>
            </div>
        </div>
    </div>
@endsection