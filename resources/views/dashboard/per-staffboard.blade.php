@extends('layouts.dash')

@section('title', 'Messages')

@section('content')
    <div class="hero">
        <div class="hero-body is-paddingless">
            <div class="message-container">
                <div class="column is-12 custom-padding">
                    <div class="box  has-padding-vertical-half new">
                        <article class="media">
                            <figure class="media-left">
                                <p class="image is-64x64">
                                    <img class="img-circle" src="http://bulma.io/images/placeholders/128x128.png">
                                </p>
                            </figure>
                            <div class="media-content">
                                <div class="content">
                                    <p class="message-container-trunc truncate">
                                        <strong>Aneth John</strong>
                                        <br>
                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin ornare magna eros
                                    </p>
                                </div>
                            </div>
                            <div class="media-right">
                                <span class="is-pulled-right">
                                12:00
                            </span>
                            </div>
                        </article>
                    </div>
                </div>
                <div class="column is-12 custom-padding">
                    <div class="box  has-padding-vertical-half">
                        <article class="media">
                            <figure class="media-left">
                                <p class="image is-64x64">
                                    <img class="img-circle" src="http://bulma.io/images/placeholders/128x128.png">
                                </p>
                            </figure>
                            <div class="media-content">
                                <div class="content">
                                    <p class="message-container-trunc truncate">
                                        <strong>Aneth John</strong>
                                        <br>
                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin ornare magna eros
                                    </p>
                                </div>
                            </div>
                            <div class="media-right">
                                <span class="is-pulled-right">
                                12:00
                            </span>
                            </div>
                        </article>
                    </div>
                </div>
                <div class="column is-12 custom-padding">
                    <div class="box  has-padding-vertical-half">
                        <article class="media">
                            <figure class="media-left">
                                <p class="image is-64x64">
                                    <img class="img-circle" src="http://bulma.io/images/placeholders/128x128.png">
                                </p>
                            </figure>
                            <div class="media-content">
                                <div class="content">
                                    <p class="message-container-trunc truncate">
                                        <strong>Aneth John</strong>
                                        <br>
                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin ornare magna eros
                                    </p>
                                </div>
                            </div>
                            <div class="media-right">
                                <span class="is-pulled-right">
                                12:00
                            </span>
                            </div>
                        </article>
                    </div>
                </div>
                <div class="column is-12 custom-padding">
                    <div class="box  has-padding-vertical-half">
                        <article class="media">
                            <figure class="media-left">
                                <p class="image is-64x64">
                                    <img class="img-circle" src="http://bulma.io/images/placeholders/128x128.png">
                                </p>
                            </figure>
                            <div class="media-content">
                                <div class="content">
                                    <p class="message-container-trunc truncate">
                                        <strong>Aneth John</strong>
                                        <br>
                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin ornare magna eros
                                    </p>
                                </div>
                            </div>
                            <div class="media-right">
                                <span class="is-pulled-right">
                                12:00
                            </span>
                            </div>
                        </article>
                    </div>
                </div>
            </div>
            <div class="chat-container">
                <div class="columns is-multiline">
                    <div class="column is-12">
                        <article class="media">
                            <figure class="media-left">
                                <p class="image is-64x64">
                                    <img src="http://bulma.io/images/placeholders/128x128.png">
                                </p>
                                <br>
                                <small>27-Sept, 2017</small>
                            </figure>
                            <div class="media-content">
                                <div class="content">
                                    <p class="message-container-trunc truncate">
                                        <strong>Gervas Mbunda</strong>
                                        <br>
                                        Lecturer
                                    </p>
                                </div>
                            </div>
                            <div class="media-right">
                                <a href="#">
                                    <span class="icon">
                                        <i class="material-icons">reply</i>
                                    </span>
                                </a>
                                <a href="#">
                                    <span class="icon">
                                        <i class="material-icons">reply_all</i>
                                    </span>
                                </a>
                                <a href="#">
                                    <span class="icon">
                                        <i class="material-icons">forward</i>
                                    </span>
                                </a>
                            </div>
                        </article>
                        <hr>
                        <article class="media">
                            <div class="media-content">
                                <div class="content">
                                    <h1 class="title">TITLE NAME TEXT</h1>
                                    <p>
                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin ornare magna eros, eu pellentesque tortor vestibulum ut. Maecenas non massa sem. Etiam finibus odio quis feugiat facilisis.
                                    </p>
                                </div>
                                <nav class="level is-mobile">
                                    <div class="level-left">
                                        <div class="level-item relative">
                                            <div class="custom is-bordered">
                                                <span class="icon absolute-left">
                                                    <i class="material-icons">insert_drive_file</i>
                                                </span>
                                                <span class="icon absolute-right">
                                                    <i class="material-icons">file_download</i>
                                                </span>
                                                <p>
                                                    DOCUMENT NAME
                                                    <br>
                                                    44KB
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </nav>
                            </div>
                        </article>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection