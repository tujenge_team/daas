@extends('layouts.dash')

@section('title', 'Messages')

@section('content')
    <div class="hero">
        <div class="hero-body is-paddingless">
            @if(isset($messages))
                @foreach($messages as $message)
                    <div class="column is-12 custom-padding">
                        <div class="box  has-padding-vertical-half">
                            <article class="media">
                                <figure class="media-left">
                                    <p class="image is-64x64">
                                        <img class="img-circle" src="{{ asset('storage/'.$message) }}">
                                    </p>
                                </figure>
                                <div class="media-content">
                                    <div class="content">
                                        <p class="truncate">
                                            <strong>{{ $message->first_name }} {{ $message->last_name }}</strong>
                                            <br>
                                            {{ $message->messages_received->first()->message }}
                                        </p>
                                    </div>
                                </div>
                                <div class="media-right">
                                    <span class="tag is-success">12</span>
                                    <br>
                                    <span class="is-pulled-right">
                            12:00
                        </span>
                                </div>
                            </article>
                        </div>
                    </div>
                @endforeach
            @endif
        </div>
    </div>
@endsection