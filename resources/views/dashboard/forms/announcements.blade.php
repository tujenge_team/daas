@extends('layouts.dash')

@section('title', 'Announcements')

@section('content')
    <announcement-form></announcement-form>
@endsection