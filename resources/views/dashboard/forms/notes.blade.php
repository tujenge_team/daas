@extends('layouts.dash')

@section('title', 'Notes')

@section('content')
    <div class="hero">
        <div class="hero-body">
            {!! Form::open() !!}
                <div class="field is-horizontal">
                    <div class="field-label">
                        {!! Form::label('course', null, ['class'=>'label']) !!}
                    </div>
                    <div class="field-body">
                        <div class="field is-narrow">
                            <div class="control">
                                <div class="select is-fullwidth">
                                    {!! Form::select('course_id', ['1' => 'Economics', '2' => 'Engineering'], null, ['class' => 'select']) !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="field is-horizontal">
                    <div class="field-label">
                        {!! Form::label('subject', null, ['class'=>'label']) !!}
                    </div>
                    <div class="field-body">
                        <div class="field is-narrow">
                            <div class="control">
                                <div class="select is-fullwidth">
                                    {!! Form::select('subject_id', ['1' => 'Economics 101', '2' => 'Engineering 101'], null, ['class' => 'select']) !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="field is-horizontal">
                    <div class="field-label">
                        {!! Form::label('title', null , ['class' => 'label']) !!}
                    </div>
                    <div class="field-body">
                        <div class="field">
                            <div class="control">
                                {!! Form::text('title', null, ['class' => 'input', 'placeholder' => 'Write title']) !!}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="field is-horizontal">
                    <div class="field-label">
                        {!! Form::label('description', null, ['class' => 'label']) !!}
                    </div>
                    <div class="field-body">
                        <div class="field">
                            <div class="control">
                                {!! Form::textarea('description', null, ['class' => 'textarea', 'placeholder' => 'Write title']) !!}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="field is-grouped is-grouped-right">
                    <p class="control">
                        <a class="button is-danger">
                            Decline
                        </a>
                    </p>
                    <p class="control">
                        <a class="button is-primary">
                            Send
                        </a>
                    </p>
                </div>
            {!! Form::close() !!}
        </div>
    </div>
@endsection