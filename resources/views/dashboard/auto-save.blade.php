@extends('layouts.dash')

@section('title', 'Auto Save')

@section('content')
    <div class="hero">
        <div class="hero-body is-paddingless">
            {{--<div class="container">--}}
            <div class="columns is-multiline">
                <div class="column is-12 custom-padding">
                    <div class="box has-padding-left-4 has-padding-vertical-half">
                            <span class="icon custom-icon">
                                <i class="material-icons has-margin-right-2">assignment</i>
                                B'com
                            </span>
                        <span class="is-pulled-right">
                                12:30
                            </span>
                    </div>
                </div>
                <div class="column is-12 custom-padding">
                    <div class="box has-padding-left-4 has-padding-vertical-half">
                            <span class="icon custom-icon">
                                <i class="material-icons has-margin-right-2">assignment</i>
                                B'com
                            </span>
                        <span class="is-pulled-right">
                                12:30
                            </span>
                    </div>
                </div>
                <div class="column is-12 custom-padding">
                    <div class="box has-padding-left-4 has-padding-vertical-half">
                            <span class="icon custom-icon">
                                <i class="material-icons has-margin-right-2">assignment</i>
                                B'com
                            </span>
                        <span class="is-pulled-right">
                                12:30
                            </span>
                    </div>
                </div>
                <div class="column is-12 custom-padding">
                    <div class="box has-padding-left-4 has-padding-vertical-half">
                            <span class="icon custom-icon">
                                <i class="material-icons has-margin-right-2">assignment</i>
                                B'com
                            </span>
                        <span class="is-pulled-right">
                                12:30
                            </span>
                    </div>
                </div>
            </div>
            {{--</div>--}}
        </div>
    </div>
@endsection