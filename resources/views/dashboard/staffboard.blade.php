@extends('layouts.dash')

@section('title', 'Staff Board')

@section('content')
    <div class="hero">
        <div class="hero-body is-paddingless">
            {{--<div class="container">--}}
                <div class="columns is-multiline">
                    @foreach($news as $new)
                        <div class="column is-12 custom-padding">
                            <div class="box has-padding-vertical-half">
                            <span class="icon custom-icon">
                                <i class="material-icons has-margin-right-2">assignment</i>
                            </span>
                                <a href="{{ route('per-staffboard', $new->id) }}">{{ $new->title }}</a>
                            <span class="is-pulled-right">
                                {{ date_format(date_create($new->created_at), "h:i") }}
                            </span>
                            </div>
                        </div>
                    @endforeach
                </div>
            {{--</div>--}}
        </div>
    </div>
@endsection