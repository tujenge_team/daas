@extends('layouts.dash')

@section('title', 'University Setup')

@section('content')
    <university-setup :university-type="{{ $types }}" :faculty-data="{{ $faculties }}" :course-data="{{ $courses }}"></university-setup>
@endsection