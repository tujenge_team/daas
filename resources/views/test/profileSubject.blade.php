@extends('layouts.container')

@section('content')
    <div class="hero">
        <div class="hero-body">
            <div class="container">
                {{--this will display the list of all types of universities--}}
                <div class="columns is-multiline">
                    @foreach($profiles as $profile)
                        <div class="column is-4">
                            <article class="message">
                                <div class="message-header">
                                    <p>{{ $profile->first_name }} {{ $profile->middle_name }} {{ $profile->last_name }}</p>
                                    <ul>
                                        @foreach($profile->subjects as $subject)
                                            <li class="control">
                                                {{ $subject->name }}
                                                <div class="button-group">
                                                    {!! Form::open(['route' => ['role_user.destroy', $profile->id ], 'method' => 'delete']) !!}
                                                    <button type="submit" class="delete is-primary"></button>
                                                    {!! Form::close() !!}
                                                    {!! Form::open(['route' => ['role_user.edit', $profile->id], 'method' => 'get']) !!}
                                                    <button class="delete">
                                                <span class="icon">
                                                    <span class="fa fa-pencil"></span>
                                                </span>
                                                    </button>
                                                    {!! Form::close() !!}
                                                </div>
                                            </li>

                                        @endforeach
                                    </ul>
                                </div>
                            </article>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
@endsection