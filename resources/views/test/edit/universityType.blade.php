@extends('layouts.container')

@section('content')
    <div class="hero">
        <div class="hero-body">
            <div class="container">
                {{--display just one univeristy type--}}
                <div class="columns is-multiline">
                    <div class="column is-4">
                        @include('forms.universityType', $type)
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection