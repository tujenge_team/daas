@extends('layouts.container')

@section('content')
    <div class="hero">
        <div class="hero-body">
            <div class="container">
                {{--this will display the list of all types of universities--}}
                <div class="columns is-multiline">
                    @foreach($types as $type)
                        <div class="column is-4">
                            <article class="message">
                                <div class="message-header">
                                    <p>{{ $type->type }}</p>
                                    <div class="button-group">
                                        {!! Form::open(['route' => ['media_type.destroy', $type->id ], 'method' => 'delete']) !!}
                                        <button type="submit" class="delete is-primary"></button>
                                        {!! Form::close() !!}
                                        {!! Form::open(['route' => ['media_type.edit', $type->id], 'method' => 'get']) !!}
                                        <button class="delete">
                                                <span class="icon">
                                                    <span class="fa fa-pencil"></span>
                                                </span>
                                        </button>
                                        {!! Form::close() !!}
                                    </div>
                                </div>
                            </article>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
@endsection