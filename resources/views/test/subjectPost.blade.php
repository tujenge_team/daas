@extends('layouts.container')

@section('content')
    <div class="hero">
        <div class="hero-body">
            <div class="container">
                {{--this will display the list of all types of universities--}}
                <div class="columns is-multiline">
                    @foreach($posts as $post)
                        <div class="column is-4">
                            <article class="message">
                                <div class="message-header">
                                    <div class="button-group">
                                        {!! Form::open(['route' => ['subject-post.destroy', $post->id ], 'method' => 'delete']) !!}
                                        <button type="submit" class="delete is-primary"></button>
                                        {!! Form::close() !!}
                                        {!! Form::open(['route' => ['subject-post.edit', $post->id], 'method' => 'get']) !!}
                                        <button class="delete">
                                                <span class="icon">
                                                    <span class="fa fa-pencil"></span>
                                                </span>
                                        </button>
                                        {!! Form::close() !!}
                                    </div>
                                </div>
                                <div class="message-content">
                                    <p class="h1 is-4">{{ $post->title }}</p>
                                    <p class="subtitle">{{ $post->content }}</p>
                                    <code>Category: {{ $post->category->name }}</code>
                                    <cite>Subject: {{ $post->subject['name'] }}</cite>
                                </div>
                            </article>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
@endsection