@extends('layouts.container')

@section('content')
    <div class="hero">
        <div class="hero-body">
            <div class="container">
                {{--this will display the list of all types of universities--}}
                <div class="columns is-multiline">
                    @foreach($resources as $resource)
                        <div class="column is-4">
                            <article class="message">
                                <div class="message-header">
                                    <p>{{ $resource->title }}</p>
                                    <p>{{ $resource->content }}</p>
                                    @foreach($resource->medias as $media)
                                        <figure class="is-image">
                                            <img src="{{ asset(\Storage::url($media->media)) }}" alt="">
                                        </figure>
                                    @endforeach
                                    <div class="button-group">
                                        {!! Form::open(['route' => ['resource.destroy', $resource->id ], 'method' => 'delete']) !!}
                                        <button type="submit" class="delete is-primary"></button>
                                        {!! Form::close() !!}
                                        {!! Form::open(['route' => ['resource.edit', $resource->id], 'method' => 'get']) !!}
                                        <button class="delete">
                                                <span class="icon">
                                                    <span class="fa fa-pencil"></span>
                                                </span>
                                        </button>
                                        {!! Form::close() !!}
                                    </div>
                                </div>
                            </article>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
@endsection