@extends('layouts.container')

@section('content')
    <div class="hero">
        <div class="hero-body">
            <div class="container">
                {{--this will display the list of all types of universities--}}
                <div class="columns is-multiline">
                    @foreach($associations as $association)
                        <div class="column is-12">
                            <article class="message">
                                <div class="message-header">
                                    <p>Owner: {{ $association->profile->first_name }} {{ $association->profile->middle_name }} {{ $association->profile->last_name }}</p>
                                    <img src="{{ asset('public'.$association->avatar) }}" alt="">
                                    <p>{{ $association->phone }}</p>
                                    <p>{{ $association->email }}</p>
                                    <p>{{ $association->website }}</p>
                                    <p>{{ $association->address }}</p>
                                    <p>{{ $association->description }}</p>
                                    <div class="button-group">
                                        {!! Form::open(['route' => ['association.destroy', $association->id ], 'method' => 'delete']) !!}
                                        <button type="submit" class="delete is-primary"></button>
                                        {!! Form::close() !!}
                                        {!! Form::open(['route' => ['association.edit', $association->id], 'method' => 'get']) !!}
                                        <button class="delete">
                                                <span class="icon">
                                                    <span class="fa fa-pencil"></span>
                                                </span>
                                        </button>
                                        {!! Form::close() !!}
                                    </div>
                                </div>
                            </article>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
@endsection