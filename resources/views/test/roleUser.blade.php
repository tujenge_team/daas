@extends('layouts.container')

@section('content')
    <div class="hero">
        <div class="hero-body">
            <div class="container">
                {{--this will display the list of all types of universities--}}
                <div class="columns is-multiline">
                    @foreach($users as $user)
                        <div class="column is-4">
                            <article class="message">
                                <div class="message-header">
                                    <p>{{ $user->name }}</p>
                                    <ul>
                                    @foreach($user->roles as $role)
                                        <li class="control">
                                            {{ $role->name }}
                                            <div class="button-group">
                                                {!! Form::open(['route' => ['role_user.destroy', $user->id ], 'method' => 'delete']) !!}
                                                <button type="submit" class="delete is-primary"></button>
                                                {!! Form::close() !!}
                                                {!! Form::open(['route' => ['role_user.edit', $user->id], 'method' => 'get']) !!}
                                                <button class="delete">
                                                <span class="icon">
                                                    <span class="fa fa-pencil"></span>
                                                </span>
                                                </button>
                                                {!! Form::close() !!}
                                            </div>
                                        </li>

                                    @endforeach
                                    </ul>
                                </div>
                            </article>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
@endsection