<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    {{--icons--}}
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/styles.css') }}" rel="stylesheet">
</head>
<body>
<div id="app">
    {{--top nav--}}
    <nav class="navbar has-shadow">
        <div class="navbar-brand">
            <div class="nav-item">
                <a href="#">
                    <i class="material-icons">dashboard</i>
                </a>
            </div>
            <a class="navbar-item" href="{{ url('/') }}">
                <img src="http://bulma.io/images/bulma-logo.png" alt="Bulma: a modern CSS framework based on Flexbox" width="112" height="28">
            </a>
            <div class="navbar-burger burger" data-target="navMenuExample">
                <span></span>
                <span></span>
                <span></span>
            </div>
        </div>

        <div id="navMenuExample" class="navbar-menu">
            <div class="navbar-end">
                <div class="navbar-item">
                    <a href="#" class="is-grey">
                        <i class="material-icons">settings</i>
                    </a>
                </div>
                <div class="navbar-item">
                    <a href="#">
                        <span class="icon is-accent">
                        <i class="material-icons">account_circle</i>
                        </span>
                    </a>
                </div>
            </div>
        </div>
    </nav>
    {{--end top nav--}}
    {{--begin icon-menu--}}
    <aside class="menu icon-menu">
       <ul class="menu-list">
        <li><a href="{{ route('dashboard') }}">
                <span class="icon">
                    <i class="material-icons">home</i>
                </span>
            </a>
        </li>
    </ul>
    </aside>
    {{--end icon-menu--}}
    {{--begin detailed menu--}}
    <aside class="menu detailed-menu">
        <ul class="menu-list">
        <li>
            <ul>
                <a href="#" class="is-active">SET UP</a>
                <li><a href="{{ route('university-setup') }}">University Setup</a></li>
                <li class="aside-has-dropdown">
                    <a>Registration</a>
                    <ul class="aside-dropdown">
                        <li><a href="{{ route('staff-registration') }}">Staff Regsitration</a></li>
                        <li><a href="{{ route('student-registration') }}">Student Registration</a></li>
                    </ul>
                </li>
            </ul>
        </li>
        <li>
            <a class="is-active">DEPARTMENT</a>
            <ul>
                <li><a href="{{ route('staffboard') }}">Staff Board</a></li>
                <li><a href="{{ route('messages') }}">Messages <span class="tag">4</span></a></li>
                <li><a href="{{ route('auto-save') }}">Auto saved</a></li>
                <li><a>History <span class="tag">4</span></a></li>
            </ul>
        </li>
        <li>
            <a class="is-active">STUDENT</a>
            <ul>
                <li class="aside-has-dropdown active">
                    <a>Class Room</a>
                    <ul class="aside-dropdown">
                        <li><a href="{{ route('announcements') }}">Announcements</a></li>
                        <li><a href="{{ route('notes') }}">Notes</a></li>
                        <li><a href="{{ route('assignments') }}">Assignments</a></li>
                        <li><a href="{{ route('course-work') }}">Course Work</a></li>
                    </ul>
                </li>
                <li><a href="{{ route('messages') }}">Messages <span class="tag">4</span></a></li>
                <li><a>Auto saved</a></li>
                <li><a>History <span class="tag">4</span></a></li>
            </ul>
        </li>
    </ul>
    </aside>
    {{--end detailed menu--}}
    {{--<nav class="navbar navbar-default navbar-static-top">--}}
        {{--<div class="container">--}}
            {{--<div class="navbar-header">--}}

                {{--<!-- Collapsed Hamburger -->--}}
                {{--<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">--}}
                    {{--<span class="sr-only">Toggle Navigation</span>--}}
                    {{--<span class="icon-bar"></span>--}}
                    {{--<span class="icon-bar"></span>--}}
                    {{--<span class="icon-bar"></span>--}}
                {{--</button>--}}

                {{--<!-- Branding Image -->--}}
                {{--<a class="navbar-brand" href="{{ url('/') }}">--}}
                    {{--{{ config('app.name', 'Laravel') }}--}}
                {{--</a>--}}
            {{--</div>--}}

            {{--<div class="collapse navbar-collapse" id="app-navbar-collapse">--}}
                {{--<!-- Left Side Of Navbar -->--}}
                {{--<ul class="nav navbar-nav">--}}
                    {{--&nbsp;--}}
                {{--</ul>--}}

                {{--<!-- Right Side Of Navbar -->--}}
                {{--<ul class="nav navbar-nav navbar-right">--}}
                    {{--<!-- Authentication Links -->--}}
                    {{--@if (Auth::guest())--}}
                        {{--<li><a href="{{ route('login') }}">Login</a></li>--}}
                        {{--<li><a href="{{ route('register') }}">Register</a></li>--}}
                    {{--@else--}}
                        {{--<li class="dropdown">--}}
                            {{--<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">--}}
                                {{--{{ Auth::user()->name }} <span class="caret"></span>--}}
                            {{--</a>--}}

                            {{--<ul class="dropdown-menu" role="menu">--}}
                                {{--<li>--}}
                                    {{--<a href="{{ route('logout') }}"--}}
                                       {{--onclick="event.preventDefault();--}}
                                                     {{--document.getElementById('logout-form').submit();">--}}
                                        {{--Logout--}}
                                    {{--</a>--}}

                                    {{--<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">--}}
                                        {{--{{ csrf_field() }}--}}
                                    {{--</form>--}}
                                {{--</li>--}}
                            {{--</ul>--}}
                        {{--</li>--}}
                    {{--@endif--}}
                {{--</ul>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</nav>--}}
    <div class="block is-title-bar is-primary is-fullwidth">
        <h1 class="title">@yield('title')</h1>
    </div>
    <div class="main-content">
        @yield('content')
    </div>
</div>

<!-- Scripts -->
<script src="{{ asset('js/app.js') }}"></script>
</body>
</html>
