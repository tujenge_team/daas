<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>DAAS</title>

    {{--css--}}
    <link rel="stylesheet" href="{{ asset('css/bulma.css') }}">

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

    {{--icons--}}
    <script src="https://use.fontawesome.com/cbb1fa18b0.js"></script>
</head>
<body>
@yield('content')
</body>
</html>
