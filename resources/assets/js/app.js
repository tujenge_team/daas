
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');
window.Buefy = require('buefy');
window.debounce = require('debounce');
window.Vue = require('vue');
Vue.use(Buefy.default);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

// Vue.component('example', require('./components/Example.vue'));
// Vue.component('passport-clients', require('./components/passport/Clients.vue'));
// Vue.component('passport-authorized-clients', require('./components/passport/AuthorizedClients.vue'));
// Vue.component('passport-personal-access-tokens', require('./components/passport/PersonalAccessTokens.vue'));
Vue.component('student-messages', require('./components/students/Messages.vue'));
Vue.component('announcement-form', require('./components/forms/Announcements.vue'));
Vue.component('university-setup', require('./components/forms/UniversitySetup.vue'));
Vue.component('staff-registration', require('./components/forms/StaffRegistraton.vue'));
Vue.component('student-registration', require('./components/forms/StudentRegistration.vue'));

const app = new Vue({
    el: '#app',
    data:  function () {
        return {
            show: false,
            profileId: 1
        }
    },
    methods: {
        toggleChat: function (id) {
            this.profileId = id;
            if(this.show) {
                return;
            }
            this.show = !this.show;
        }
    },
    computed: {
        getProfileId: function () {
            return this.profileId;
        }
    }
});
